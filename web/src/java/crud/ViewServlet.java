/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
    import java.util.List;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/ViewServlet")  
    public class ViewServlet extends HttpServlet {  
        protected void doGet(HttpServletRequest request, HttpServletResponse response)   
                   throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
            out.println("<a href='Datos_Legajos.html'>Agregar empleado</a>");
            out.println("<h1>Lista de Empleados</h1>");  
              
            List<Emp> list=EmpDao.getAllEmployees();  
              
            out.print("<table border='1' width='100%'");  
            out.print("<tr><th>Id</th><th>Nombre</th><th>Apellido</th><th>Dni</th><th>Sector</th><th>Telefono</th><th>Interno</th><th>Legajo</th><th>Password</th><th>Admin</th><th>País</th><th>Email</th><th>Editar</th><th>Borrar</th></tr>");  
            for(Emp e:list){  
             out.print("<tr><td>"+e.getId()+"</td><td>"+e.getName()+"</td><td>"+e.getLastname()+"</td><td>"+e.getDni()+"</td><td>"+e.getSector()+"</td><td>"+e.getPhone()+"</td><td>"+e.getExtension()+"</td><td>"+e.getEmployeenumber()+"</td><td>"+e.getPassword()+"</td><td>"+e.getAdmin()+"</td><td>"+e.getCountry()+"</td><td>"+e.getEmail()+"</td><td><a href='EditServlet?id="+e.getId()+"'>Editar</a></td><td><a href='DeleteServlet?id="+e.getId()+"'>Eliminar</a></td></tr>");  
            }  
            out.print("</table>");  
              
            out.close();  
        }  
    }  