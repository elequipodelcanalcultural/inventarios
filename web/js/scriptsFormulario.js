/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function insertarMensaje(input, claseMensaje, mensaje) {
    var elementoMsj = accederAElemento('msj_' + input.id);
    if (!elementoMsj) {
        var nuevoElemento = document.createElement('div');
        nuevoElemento.id = 'msj_' + input.id;
        nuevoElemento.className = claseMensaje;
        nuevoElemento.innerHTML = mensaje;
        input.parentElement.appendChild(nuevoElemento);
    } else {
        elementoMsj.className = claseMensaje;
        elementoMsj.innerHTML = mensaje;
    }
}


function usuarioValido() {
    var elemento = accederAElemento('inputUsuario');
    if (estaCompleto(elemento.value) && noTieneEspacios(elemento.value) && largoMayorQue(elemento.value, 6)) {
        insertarMensaje(elemento, 'mensaje_valido', '¡Usuario válido!');
        return true;
    } else {
        insertarMensaje(elemento, 'mensaje_invalido', '¡Usuario inválido!');
        return false;
    }
}



function passwordValido() {
    var elemento = accederAElemento('inputPassword');
    if (estaCompleto(elemento.value) && noTieneEspacios(elemento.value) && 
            tieneMayuscula(elemento.value) && tieneMinuscula(elemento.value) &&
            tieneNumero(elemento.value) && largoMayorQue(elemento.value, 5)) {
        insertarMensaje(elemento, 'mensaje_valido', '¡Password válido!');
        return true;
    } else {
        insertarMensaje(elemento, 'mensaje_invalido', '¡Password inválido!');
        return false;
    }
}


function loginValido() {
    var usuario = accederAElemento('inputUsuario');
    var password = accederAElemento('inputPassword');
    if (usuarioValido() && passwordValido() && usuario.value === 'Adminequipos' && 
        password.value === 'Ifts16') {
        alert('Usuario y contraseña correctos. A continuación se enviará a la página principal');
        //window.location.href = 'Usuarios.html';
        //window.location.href = 'http://localhost/login.jsp';
        document.querySelector('#formularioLogin').submit();
    } else {
        alert('Usuario y/o contraseña incorrectos. Por favor verifique los datos ingresados');       
    }
}
