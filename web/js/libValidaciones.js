/**
  Esta librería proporciona funciones básicas para validar
  formularios. Cada función tiene por nombre una pregunta
  que se puede responder por sí o por no. Normalmente 
  esperamos que la función devuelva true para darla como
  correcta y false incorrecta, pero cómo se utiliza 
  específicamente dependerá del programador.
  Las funciones obtienen los valores de un formulario
  como primer parámetro, quizá un segundo parámetro. 
  Todas devuelven valores booleanos.
  Estas funciones están pensadas para ser utilizadas en conjunto
  a fin de poder armar validaciones complejas.
**/
/* accederElemento('id_campo')
    Ésta es una función accesoria que permite acceder
    a un elemento mediante su id y guardarlo en una
    variable para utilizar sus atributos.
*/

function accederAElemento(elementoId) {
  var elemento = document.querySelector('#' + elementoId);
  return elemento;
}

/** 
  Utilización: nombreDeFuncion(input.value)
  Devuelve: true o false (siempre)
**/

/* estaCompleto(elemento.value)
    Verifica que el campo esté completo (no sirve para checks)
*/
function estaCompleto(valor) {
    if (valor.length === 0) {
      return false;
    } else {
      return true;
    }
  }



/*  noTieneEspacios(elemento.value)
      Verifica que el valor ingresado no tenga espacios.
*/
  function noTieneEspacios(valor) {
    if (!/\s/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
  
/*  esEntero(elemento.value)
      Verifica que el valor ingresado sea un número entero.
*/
  function esEntero(valor) {
    if (/^[0-9]+$/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
/* esFecha(elemento.value)
    Verifica que el valor ingresado sea una fecha válida.
    Formato: dd/mm/YYYY
    Ejemplo: 23/11/1994
*/  

 function esFecha(valor) {
    if (/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(valor)) { 
      return true;
    }  else {
      return false;
    }
  }
  
  /* esFechaISO(elemento.value)
    Verifica que el valor ingresado sea una fecha válida en formato ISO.
    Formato: YYYY-mm-dd
    Ejemplo: 1994-11-23
*/  
  function esFechaISO(valor) {
    if (/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(valor)) { 
      return true;
    }  else {
      return false;
    }
  }

/*  esEmail(elemento.value)
      Verifica que el texto introducido sea un email válido.
*/
  function esEmail(valor) {
    var er = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (er.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
  
/*  tieneMayuscula(elemento.value)
  Verifica que el texto introducido tenga al menos una letra mayúscula.
*/
  function tieneMayuscula(valor) {
    if ( /[A-Z]+/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
  
/*  tieneMinuscula(elemento.value)
      Verifica que el texto introducido tenga al menos una letra minúscula.
*/
  function tieneMinuscula(valor) {
    if ( /[a-z]+/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }

/*  tieneNumero(elemento.value)
      Verifica que el texto introducido tenga al menos un número.
*/
  function tieneNumero(valor) {
    if ( /[0-9]+/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
  
/*  tieneSimbolo(elemento.value)
      Verifica que el texto introducido tenga al menos un valor que no
      sea letra ni número  (signos de puntuación, otros).
*/  
  function tieneSimbolo(valor) {
    if ( /\W+/.test(valor)) {
      return true;
    } else {
      return false;
    }
  }
  
/*  largoMayorQue(elemento.value, 8)
      Verifica que el largo del texto introducido sea mayor o igual que el numero mínimo.
*/
  function largoMayorQue(valor, numero) {
    if ( valor.length >= numero ) {
      return true;
    } else {
      return false;
    }
  }

/*  largoMenorQue(elemento.value, 6)
      Verifica que el largo del texto introducido sea menor o igual que el número máximo.
*/
  function largoMenorQue(valor, numero) {
    if ( valor.length <= numero ) {
      return true;
    } else {
      return false;
    }
  }
  
/*  valorMayorQue(elemento.value, 13)
      Verifica que el número ingresado sea mayor que el número mínimo.
*/
function valorMayorQue(valor, numero) {
  if (parseInt(valor) >= numero) {
    return true;
  } else {
    return false;
  }
}
  
/*  valorMenorQue(elemento.value, 99)
      Verifica que el número ingresado sea menor que el número máximo.
*/
function valorMenorQue(valor, numero) {
  if (parseInt(valor) <= numero) {
    return true;
  } else {
    return false;
  }
}


  
/*  checkCompleto(elemento.value) 
      Verifica que el campo checkbox esté completo.
*/
  function checkCompleto(valor) {
    if (valor) {
      return true;
    } else {
      return false;
    }
  }
  
/*  comparar_valor(elemento.value, 'espero este texto')
      Compara un valor textual con uno que se espera.
      Ideal para usar con botones de radio cuando solo uno de los valores
      es correcto.
*/
  function esValorEsperado(valor, valorEsperado) {
    if (valor === valorEsperado) {
      return true;
    } else {
      return false;
    }
  }
  

/*  valor_existente(elemento.value, ['1 A', '1 B', '2 A', '3 A'])
      Verifica que el valor ingresado se encuentre dentro de una lista
      de valores esperada (ideal para selects).
*/
  function valorExistente(valor, arrayValores) {
    for(var i = 0; i < arrayValores.length; i++) {
        if(arrayValores[i] == valor) {
          return true;
        }
    }
    return false;
  }