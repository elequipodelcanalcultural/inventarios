/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.util.*;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
      
    public class EmpDao {  
      
        public static Connection getConnection(){  
            Connection con=null;  
            try{  
                Class.forName("com.mysql.jdbc.Driver");  
                con=DriverManager.getConnection("jdbc:mysql://localhost:3306/users",
                      "root","");  
            }catch(Exception e){System.out.println(e);}  
            return con;  
        }  
        public static int save(Emp e){  
            int status=0;  
            try(Connection con = EmpDao.getConnection()) {  
                PreparedStatement ps=con.prepareStatement(  
                             "insert into usuarios(name,lastname,dni,sector,phone,extension,employeenumber,password,admin,country,email) values (?,?,?,?,?,?,?,?,?,?,?)");  
                ps.setString(1,e.getName());  
                ps.setString(2,e.getLastname());  
                ps.setString(3,e.getDni());  
                ps.setString(4,e.getSector());  
                ps.setString(5,e.getPhone());  
                ps.setString(6,e.getExtension());  
                ps.setString(7,e.getEmployeenumber());  
                ps.setString(8,e.getPassword());  
                ps.setString(9,e.getAdmin());  
                ps.setString(10,e.getCountry());  
                ps.setString(11,e.getEmail());  
                  
                status=ps.executeUpdate();  
                  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return status;  
        }  
        public static int update(Emp e){  
            int status=0;  
            try(Connection con = EmpDao.getConnection()) {  
                PreparedStatement ps=con.prepareStatement(  
                             "update usuarios set name=?,lastname=?,dni=?,sector=?,phone=?,extension=?,employeenumber=?,password=?,admin=?,country=?,email=? where id=?");  
                ps.setString(1,e.getName());  
                ps.setString(2,e.getLastname());  
                ps.setString(3,e.getDni());  
                ps.setString(4,e.getSector());  
                ps.setString(5,e.getPhone());  
                ps.setString(6,e.getExtension());  
                ps.setString(7,e.getEmployeenumber());  
                ps.setString(8,e.getPassword());  
                ps.setString(9,e.getAdmin());  
                ps.setString(10,e.getCountry());  
                ps.setString(11,e.getEmail());  
                ps.setInt(12,e.getId());  
                  
                status=ps.executeUpdate();  
                  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return status;  
        }  
        public static int delete(int id){  
            int status=0;  
            try(Connection con = EmpDao.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("delete from usuarios where id=?");  
                ps.setInt(1,id);  
                status=ps.executeUpdate();  
                  
            }catch(Exception e){e.printStackTrace();}  
              
            return status;  
        }  
        public static Emp getEmployeeById(int id){  
            Emp e=new Emp();  
              
            try(Connection con = EmpDao.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("select * from usuarios where id=?");  
                ps.setInt(1,id);  
                ResultSet rs=ps.executeQuery();  
                if(rs.next()){  
                    e.setId(rs.getInt(1));  
                    e.setName(rs.getString(2));  
                    e.setLastname(rs.getString(3));
                    e.setDni(rs.getString(4));
                    e.setSector(rs.getString(5));
                    e.setPhone(rs.getString(6));
                    e.setExtension(rs.getString(7));
                    e.setEmployeenumber(rs.getString(8));
                    e.setPassword(rs.getString(9));
                    e.setAdmin(rs.getString(10));
                    e.setCountry(rs.getString(11));
                    e.setEmail(rs.getString(12));  
  
                }  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return e;  
        }  
        public static List<Emp> getAllEmployees(){  
            List<Emp> list=new ArrayList<Emp>();  
              
            try(Connection con = EmpDao.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("select * from usuarios");  
                ResultSet rs=ps.executeQuery();  
                while(rs.next()){  
                    Emp e=new Emp();  
                    e.setId(rs.getInt(1));  
                    e.setName(rs.getString(2));  
                    e.setLastname(rs.getString(3));
                    e.setDni(rs.getString(4));
                    e.setSector(rs.getString(5));
                    e.setPhone(rs.getString(6));
                    e.setExtension(rs.getString(7));
                    e.setEmployeenumber(rs.getString(8));
                    e.setPassword(rs.getString(9));
                    e.setAdmin(rs.getString(10));
                    e.setCountry(rs.getString(11));
                    e.setEmail(rs.getString(12));  
                      
                    list.add(e);  
                }  
            }catch(Exception e){e.printStackTrace();}  
              
            return list;  
        }  
    }  