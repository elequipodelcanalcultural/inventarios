/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/EditServlet_equipos")  
    public class EditServlet_equipos extends HttpServlet {  
        protected void doGet(HttpServletRequest request, HttpServletResponse response)   
               throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
            out.println("<h1>Actualizar Equipos</h1>");  
            String sid=request.getParameter("id");  
            int id=Integer.parseInt(sid);  
              
            Emp_equipos e=EmpDao_equipos.getEquiposById(id);  
              
            out.print("<form action='EditServlet2_equipos' method='post'>");  
            out.print("<table>");  
            out.print("<tr><td>ID</td><td><input type='hidden' name='id' value='"+e.getId()+"'/></td></tr>");  
            out.print("<tr><td>Tipo:</td><td><input type='text' name='tipo' value='"+e.getTipo()+"'/></td></tr>");  
            out.print("<tr><td>Marca:</td><td><input type='text' name='marca' value='"+e.getMarca()+"'/></td></tr>");  
            out.print("<tr><td>Modelo:</td><td><input type='text' name='modelo' value='"+e.getModelo()+"'/></td></tr>");  
            out.print("<tr><td>Serie:</td><td><input type='text' name='serie' value='"+e.getSerie()+"'/></td></tr>");  
            out.print("<tr><td>Estado:</td><td><input type='text' name='estado' value='"+e.getEstado()+"'/></td></tr>");  
            out.print("</td></tr>");  
            out.print("<tr><td colspan='2'><input type='submit' value='Editar y guardar '/></td></tr>");  
            out.print("</table>");  
            out.print("</form>");  
              
            out.close();  
        }  
    }  