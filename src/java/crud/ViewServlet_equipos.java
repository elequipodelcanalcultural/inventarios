/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
    import java.util.List;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/ViewServlet_equipos")  
    public class ViewServlet_equipos extends HttpServlet {  
        protected void doGet(HttpServletRequest request, HttpServletResponse response)   
                   throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
            out.println("<a href='Datos_equipos.html'>Agregar equipo</a>");  
            out.println("<h1>Lista de equipos</h1>");  
              
            List<Emp_equipos> list=EmpDao_equipos.getAllEquipos();  
              
            out.print("<table border='1' width='100%'");  
            out.print("<tr><th>Id</th><th>Tipo</th><th>Marca</th><th>Modelo</th><th>Serie</th><th>Estado</th><th>Editar</th><th>Borrar</th></tr>");  
       
            for(Emp_equipos e:list){  
             out.print("<tr><td>"+e.getId()+"</td><td>"+e.getTipo()+"</td><td>"+e.getMarca()+"</td><td>"+e.getModelo()+"</td><td>"+e.getSerie()+"</td><td>"+e.getEstado()+"</td><td><a href='EditServlet_equipos?id="+e.getId()+"'>Editar</a></td><td><a href='DeleteServlet_equipos?id="+e.getId()+"'>Eliminar</a></td></tr>");  
            }  
            out.print("</table>");  
              
            out.close();  
        }  
    }  