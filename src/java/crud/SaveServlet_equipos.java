/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/SaveServlet_equipos")  
    public class SaveServlet_equipos extends HttpServlet {  
        protected void doPost(HttpServletRequest request, HttpServletResponse response)   
             throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
              
            String tipo=request.getParameter("tipo");  
            String marca=request.getParameter("marca");  
            String modelo=request.getParameter("modelo");  
            String serie=request.getParameter("serie");  
            String estado=request.getParameter("estado");  
            
            
            Emp_equipos e=new Emp_equipos();  
            e.setTipo(tipo);  
            e.setMarca(marca);
            e.setModelo(modelo);
            e.setSerie(serie);
            e.setEstado(estado);
              
            int status=EmpDao_equipos.save(e);  
            
            if(status>0){  
                out.print("<p>Datos grabados correctamente!</p>");  
                request.getRequestDispatcher("Datos_equipo.html").include(request, response);  
            }else{  
                out.println("No se pudo grabar :(");  
            }  
              
            out.close();  
        }  
      
    }  
