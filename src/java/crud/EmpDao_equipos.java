/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.util.*;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
      
    public class EmpDao_equipos {  
      
        public static Connection getConnection(){  
            Connection con=null;  
            try{  
                Class.forName("com.mysql.jdbc.Driver");  
                con=DriverManager.getConnection("jdbc:mysql://localhost:3306/users",
                      "root","");  
            }catch(Exception e){System.out.println(e);}  
            return con;  
        }  
        public static int save(Emp_equipos e){  
            int status=0;  
            try(Connection con = EmpDao_equipos.getConnection()) {  
                PreparedStatement ps=con.prepareStatement(  
                             "insert into equipos(tipo,marca,modelo,serie,estado) values (?,?,?,?,?)");  
                ps.setString(1,e.getTipo());  
                ps.setString(2,e.getMarca());  
                ps.setString(3,e.getModelo());  
                ps.setString(4,e.getSerie());  
                ps.setString(5,e.getEstado());  
                  
                status=ps.executeUpdate();  
                  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return status;  
        }  
        public static int update(Emp_equipos e){  
            int status=0;  
            try(Connection con = EmpDao_equipos.getConnection()) {  
                PreparedStatement ps=con.prepareStatement(  
                             "update equipos set tipo=?,marca=?,modelo=?,serie=?,estado=? where id_equipo=?");  
                
                ps.setString(1,e.getTipo());  
                ps.setString(2,e.getMarca());  
                ps.setString(3,e.getModelo());  
                ps.setString(4,e.getSerie());  
                ps.setString(5,e.getEstado());
                  
                status=ps.executeUpdate();  
                  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return status;  
        }  
        public static int delete(int id){  
            int status=0;  
            try(Connection con = EmpDao_equipos.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("delete from equipos where id_equipo=?");  
                ps.setInt(1,id);  
                status=ps.executeUpdate();  
                  
            }catch(Exception e){e.printStackTrace();}  
              
            return status;  
        }  
        public static Emp_equipos getEquiposById(int id){  
            Emp_equipos e=new Emp_equipos();  
              
            try(Connection con = EmpDao_equipos.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("select * from equipos where id_equipo=?");  
                ps.setInt(1,id);  
                ResultSet rs=ps.executeQuery();  
                if(rs.next()){  
                    e.setId(rs.getInt(1));  
                    e.setTipo(rs.getString(2));  
                    e.setMarca(rs.getString(3));
                    e.setModelo(rs.getString(4));
                    e.setSerie(rs.getString(5));
                    e.setEstado(rs.getString(6));
                   
                }  
            }catch(Exception ex){ex.printStackTrace();}  
              
            return e;  
        }  
        public static List<Emp_equipos> getAllEquipos(){  
            List<Emp_equipos> list=new ArrayList<Emp_equipos>();  
              
            try(Connection con = EmpDao_equipos.getConnection()) {  
                PreparedStatement ps=con.prepareStatement("select * from equipos");  
                ResultSet rs=ps.executeQuery();  
                while(rs.next()){  
                    Emp_equipos e=new Emp_equipos();  
                    e.setId(rs.getInt(1));  
                    e.setTipo(rs.getString(2));  
                    e.setMarca(rs.getString(3));
                    e.setModelo(rs.getString(4));
                    e.setSerie(rs.getString(5));
                    e.setEstado(rs.getString(6));
                     
                    list.add(e);  
                }  
            }catch(Exception e){e.printStackTrace();}  
              
            return list;  
        }  
    }  