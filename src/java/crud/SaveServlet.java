/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/SaveServlet")  
    public class SaveServlet extends HttpServlet {  
        protected void doPost(HttpServletRequest request, HttpServletResponse response)   
             throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
              
            String name=request.getParameter("name");  
            String lastname=request.getParameter("lastname");  
            String dni=request.getParameter("dni");  
            String sector=request.getParameter("sector");  
            String phone=request.getParameter("phone");  
            String extension=request.getParameter("extension");  
            String employeenumber=request.getParameter("employeenumber");  
            String password=request.getParameter("password");  
            String admin=request.getParameter("admin");  
            String country=request.getParameter("country"); 
            String email=request.getParameter("email");  
            
            
            Emp e=new Emp();  
            e.setName(name);  
            e.setLastname(lastname);
            e.setDni(dni);
            e.setSector(sector);
            e.setPhone(phone);
            e.setExtension(extension);
            e.setEmployeenumber(employeenumber);
            e.setPassword(password);  
            e.setAdmin(admin);
            e.setCountry(country);  
            e.setEmail(email);  
              
            int status=EmpDao.save(e);  
            
            if(status>0){  
                out.print("<p>Datos grabados correctamente!</p>");  
                request.getRequestDispatcher("index.html").include(request, response);  
            }else{  
                out.println("No se pudo grabar :(");  
            }  
              
            out.close();  
        }  
      
    }  
