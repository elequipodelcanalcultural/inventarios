/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */

    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/EditServlet2_equipos")  
    public class EditServlet2_equipos extends HttpServlet {  
        protected void doPost(HttpServletRequest request, HttpServletResponse response)   
              throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
              
            String sid=request.getParameter("id");  
            int id=Integer.parseInt(sid);  
            String tipo=request.getParameter("tipo");  
            String marca=request.getParameter("marca");  
            String modelo=request.getParameter("modelo");  
            String serie=request.getParameter("serie");  
            String estado=request.getParameter("estado");  
              
            Emp_equipos e=new Emp_equipos();  
            e.setId(id);  
            e.setTipo(tipo);  
            e.setMarca(marca);
            e.setModelo(modelo);
            e.setSerie(serie);
            e.setEstado(estado); 
            
            
            
            
            int status=EmpDao_equipos.update(e);  
            if(status>0){  
                response.sendRedirect("ViewServlet_equipos");  
            }else{  
                out.println("Error al actualizar datos :(");  
            }  
              
            out.close();  
        }  
      
    }  