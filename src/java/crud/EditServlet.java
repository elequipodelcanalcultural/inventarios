/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author Javier
 */
    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.annotation.WebServlet;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
    @WebServlet("/EditServlet")  
    public class EditServlet extends HttpServlet {  
        protected void doGet(HttpServletRequest request, HttpServletResponse response)   
               throws ServletException, IOException {  
            response.setContentType("text/html");  
            PrintWriter out=response.getWriter();  
            out.println("<h1>Update Employee</h1>");  
            String sid=request.getParameter("id");  
            int id=Integer.parseInt(sid);  
              
            Emp e=EmpDao.getEmployeeById(id);  
              
            out.print("<form action='EditServlet2' method='post'>");  
            out.print("<table>");  
            out.print("<tr><td>ID</td><td><input type='hidden' name='id' value='"+e.getId()+"'/></td></tr>");  
            out.print("<tr><td>Nombre:</td><td><input type='text' name='name' value='"+e.getName()+"'/></td></tr>");  
            out.print("<tr><td>Apellido:</td><td><input type='text' name='lastname' value='"+e.getLastname()+"'/></td></tr>");  
            out.print("<tr><td>DNI:</td><td><input type='text' name='dni' value='"+e.getDni()+"'/></td></tr>");  
            out.print("<tr><td>Sector:</td><td><input type='text' name='sector' value='"+e.getSector()+"'/></td></tr>");  
            out.print("<tr><td>Telefono:</td><td><input type='text' name='phone' value='"+e.getPhone()+"'/></td></tr>");  
            out.print("<tr><td>Interno:</td><td><input type='text' name='extension' value='"+e.getExtension()+"'/></td></tr>");  
            out.print("<tr><td>Legajo:</td><td><input type='text' name='employeenumber' value='"+e.getEmployeenumber()+"'/></td></tr>");  
            out.print("<tr><td>Password:</td><td><input type='password' name='password' value='"+e.getPassword()+"'/> </td></tr>");  
            out.print("<tr><td>País:</td><td>");  
            out.print("<select name='country' style='width:150px' value='"+e.getCountry()+"'>");  
            out.print("<option>India</option>");  
            out.print("<option>USA</option>");  
            out.print("<option>UK</option>");  
            out.print("<option>Otro</option>");  
            out.print("</select>");  
            out.print("<tr><td>Email:</td><td><input type='email' name='email' value='"+e.getEmail()+"'/></td></tr>");
            out.print("</td></tr>");  
            out.print("<tr><td colspan='2'><input type='submit' value='Editar y guardar '/></td></tr>");  
            out.print("</table>");  
            out.print("</form>");  
              
            out.close();  
        }  
    }  