<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'/>
        <link href="Css/estilos.css" rel="stylesheet" type="text/css"/>
        
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mostrar todos los usuarios</title>
</head>
<body>
    <table border=1>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dni</th>
                <th>Email</th>
                <th>Sector</th>
                <th>Interno</th>
                <th>Perfil</th>
                <th>Password</th>
                <th>Registrado en</th>
                <th colspan=2>Accion</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${usuarios}" var="user">
                <tr>
                    <td><c:out value="${user.usu_id}"/></td>
                    <td><c:out value="${user.usu_nombre}"/></td>
                    <td><c:out value="${user.usu_apellido}"/></td>
                    <td><c:out value="${user.usu_dni}"/></td>
                    <td><c:out value="${user.usu_email}"/></td>
                    <td><c:out value="${user.usu_sector}"/></td>
                    <td><c:out value="${user.usu_interno}"/></td>
                    <td><c:out value="${user.usu_perfil}"/></td>
                    <td><c:out value="${user.usu_password}"/></td>
                    <td><fmt:formatDate pattern="dd MMM,yyyy" value="${user.registeredon}" /></td>
                    <td><a href="UserController?action=editar&usu_id=<c:out value="${user.usu_id}"/>">Actualizar</a></td>
                    <td><a href="UserController?action=delete&usu_id=<c:out value="${user.usu_id}"/>">Borrar</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p><a href="UserController?action=insert">Agregar Usuario</a></p>
</body>
</html>
