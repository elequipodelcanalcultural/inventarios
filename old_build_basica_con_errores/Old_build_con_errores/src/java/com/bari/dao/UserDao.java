package com.bari.dao;

import java.sql.*;
import java.util.*;
import com.bari.model.User;
import com.bari.util.Database;

public class UserDao {

    private Connection connection;

    public UserDao() {
        connection = Database.getConnection();
    }
//lo que viene acá abajo chequea si existe el usuario en base al nombre, 
//pero cómo hago si el nombre está repetido? tendría que validar en base al ID y al nombre, no?
//    
    public void checkUser(User user) {
        try {
            PreparedStatement ps = connection.prepareStatement("select usu_id from usuarios where usu_id = ?");
            ps.setString(1, user.getUsu_id());
            ResultSet rs = ps.executeQuery();
            if (rs.next())
            {
                updateUser(user);
            } else {
                addUser(user);
            }
        } catch (Exception ex) {
            System.out.println("Error in check() -->" + ex.getMessage());
        } 
    }
    public void addUser(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into usuarios(usu_nombre, usu_apellido, usu_dni, usu_email, usu_sector, usu_interno, usu_perfil, usu_password, registeredon) values (?, ?, ?, ?, ?, ?, ?, ? )");
            // Los parametros tienen que arrancar con 1
            preparedStatement.setString(1, user.getUsu_nombre());
            preparedStatement.setString(2, user.getUsu_apellido());
            preparedStatement.setString(3, user.getUsu_dni());
            preparedStatement.setString(4, user.getUsu_email());  
            preparedStatement.setString(5, user.getUsu_sector());
            preparedStatement.setString(6, user.getUsu_interno());
            preparedStatement.setString(7, user.getUsu_perfil());
            preparedStatement.setString(8, user.getUsu_password());
            preparedStatement.setDate(9, new java.sql.Date(user.getRegisteredon().getTime()));
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(String usu_id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from usuarios where usu_id=?");
            // Los parametros tienen que arrancar con 1
            preparedStatement.setString(1, usu_id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("update usuarios set usu_nombre=?, usu_apellido=?, usu_dni=?, usu_email=?, usu_sector=?, usu_interno=?, usu_perfil=?, usu_password=?"
                    + "where usu_id=?");
            // Los parametros tienen que arrancar con 1
            System.out.println(new java.sql.Date(user.getRegisteredon().getTime()));
            preparedStatement.setString(1, user.getUsu_nombre());
            preparedStatement.setString(2, user.getUsu_apellido());
            preparedStatement.setString(4, user.getUsu_dni());
            preparedStatement.setString(4, user.getUsu_email());
            preparedStatement.setString(5, user.getUsu_sector());
            preparedStatement.setString(6, user.getUsu_interno());
            preparedStatement.setString(7, user.getUsu_perfil());
            preparedStatement.setString(8, user.getUsu_interno());
            preparedStatement.setString(9, user.getUsu_password());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from usuarios");
            while (rs.next()) {
                User user = new User();
                user.setUsu_id(rs.getString("usu_id"));  //Parece que ahora anda 
                user.setUsu_nombre(rs.getString("usu_nombre"));
                user.setUsu_apellido(rs.getString("usu_apellido"));
                user.setUsu_dni(rs.getString("usu_dni"));
                user.setUsu_email(rs.getString("usu_email"));
                user.setUsu_sector(rs.getString("usu_sector"));
                user.setUsu_interno(rs.getString("usu_interno"));
                user.setUsu_perfil(rs.getString("usu_perfil"));
                user.setUsu_password(rs.getString("usu_password"));
                user.setRegisteredon(rs.getDate("registeredon"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public User getUserById(String userId) {
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from usuarios where usu_id=?");
            preparedStatement.setString(1, userId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user.setUsu_nombre(rs.getString("usu_nombre"));
                user.setUsu_email(rs.getString("usu_apellido"));
                user.setUsu_email(rs.getString("usu_dni"));
                user.setUsu_email(rs.getString("usu_email"));
                user.setUsu_email(rs.getString("usu_sector"));
                user.setUsu_email(rs.getString("usu_interno"));
                user.setUsu_email(rs.getString("usu_perfil"));
                user.setUsu_password(rs.getString("usu_password"));
                user.setRegisteredon(rs.getDate("registeredon"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    private String getString(String usu_id) {
        throw new UnsupportedOperationException("Not se puede traer USU_ID"); // Error cuando no puede traer el ID
    }
}
