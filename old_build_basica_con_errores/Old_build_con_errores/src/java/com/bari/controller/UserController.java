package com.bari.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bari.dao.UserDao;
import com.bari.model.User;

public class UserController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/user.jsp";
    private static String LIST_USER = "/listuser.jsp";
    private UserDao dao;

    public UserController() {
        super();
        dao = new UserDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){ //si le das delete, completa la acción y lista los usuarios
            String usu_id = request.getParameter("usu_id");
            dao.deleteUser(usu_id);
            forward = LIST_USER;
            request.setAttribute("usuarios", dao.getAllUsers());    
        } else if (action.equalsIgnoreCase("editar")){ //si le das editar o insertar...
            forward = INSERT_OR_EDIT;
            String usu_id = request.getParameter("usu_id"); //toma el valor de usu_id de la pagina anterior
            User user = dao.getUserById(usu_id);
            request.setAttribute("usuario", user); 
        } else if (action.equalsIgnoreCase("listuser")){
            forward = LIST_USER;
            request.setAttribute("usuarios", dao.getAllUsers());
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        user.setUsu_nombre(request.getParameter("usu_nombre"));
        user.setUsu_password(request.getParameter("usu_password"));
        try {
            Date reg = new SimpleDateFormat("yyyy/MM/dd").parse(request.getParameter("dob"));
            System.out.println("rrrrrrrrrrr"+ reg);
            user.setRegisteredon(reg);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        user.setUsu_email(request.getParameter("usu_email"));
        String usu_id = request.getParameter("usu_id");

        
            user.setUsu_nombre(user);
            dao.checkUser(user);

            
        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        request.setAttribute("usuarios", dao.getAllUsers());
        view.forward(request, response);
    }
}
