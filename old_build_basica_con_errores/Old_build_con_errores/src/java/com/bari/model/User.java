package com.bari.model;

import java.util.Date;

public class User {

    private String usu_nombre;
    private String usu_apellido;
    private String usu_dni;
    private String usu_email;
    private String usu_sector;
    private String usu_interno;
    private String usu_img;
    private String usu_perfil;
    private String usu_password;
    private Date registeredon;
    private String usu_id;

    /**
     * @return the usu_nombre
     */
    public String getUsu_nombre() {
        return usu_nombre;
    }

    /**
     * @param usu_nombre the usu_nombre to set
     */
    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    /**
     * @return the usu_apellido
     */
    public String getUsu_apellido() {
        return usu_apellido;
    }

    /**
     * @param usu_apellido the usu_apellido to set
     */
    public void setUsu_apellido(String usu_apellido) {
        this.usu_apellido = usu_apellido;
    }

    /**
     * @return the usu_dni
     */
    public String getUsu_dni() {
        return usu_dni;
    }

    /**
     * @param usu_dni the usu_dni to set
     */
    public void setUsu_dni(String usu_dni) {
        this.usu_dni = usu_dni;
    }

    /**
     * @return the usu_email
     */
    public String getUsu_email() {
        return usu_email;
    }

    /**
     * @param usu_email the usu_email to set
     */
    public void setUsu_email(String usu_email) {
        this.usu_email = usu_email;
    }

    /**
     * @return the usu_sector
     */
    public String getUsu_sector() {
        return usu_sector;
    }

    /**
     * @param usu_sector the usu_sector to set
     */
    public void setUsu_sector(String usu_sector) {
        this.usu_sector = usu_sector;
    }

    /**
     * @return the usu_interno
     */
    public String getUsu_interno() {
        return usu_interno;
    }

    /**
     * @param usu_interno the usu_interno to set
     */
    public void setUsu_interno(String usu_interno) {
        this.usu_interno = usu_interno;
    }

    /**
     * @return the usu_img
     */
    public String getUsu_img() {
        return usu_img;
    }

    /**
     * @param usu_img the usu_img to set
     */
    public void setUsu_img(String usu_img) {
        this.usu_img = usu_img;
    }

    /**
     * @return the usu_perfil
     */
    public String getUsu_perfil() {
        return usu_perfil;
    }

    /**
     * @param usu_perfil the usu_perfil to set
     */
    public void setUsu_perfil(String usu_perfil) {
        this.usu_perfil = usu_perfil;
    }

    /**
     * @return the usu_password
     */
    public String getUsu_password() {
        return usu_password;
    }

    /**
     * @param usu_password the usu_password to set
     */
    public void setUsu_password(String usu_password) {
        this.usu_password = usu_password;
    }

    /**
     * @return the registeredon
     */
    public Date getRegisteredon() {
        return registeredon;
    }

    /**
     * @param registeredon the registeredon to set
     */
    public void setRegisteredon(Date registeredon) {
        this.registeredon = registeredon;
    }

    /**
     * @return the usu_id
     */
    public String getUsu_id() {
        return usu_id;
    }

    /**
     * @param usu_id the usu_id to set
     */
    public void setUsu_id(String usu_id) {
        this.usu_id = usu_id;
    }

    public void setUsu_nombre(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
